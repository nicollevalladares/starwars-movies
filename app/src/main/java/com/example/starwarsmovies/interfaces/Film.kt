package com.example.starwarsmovies.interfaces

class Film (val title: String, val director: String, val openingCrawl: String, val objectId: String, val episodeId: String)