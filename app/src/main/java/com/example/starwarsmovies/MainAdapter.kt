package com.example.starwarsmovies

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.starwarsmovies.interfaces.ResponseAPI
import kotlinx.android.synthetic.main.film_item.view.*

class MainAdapter(val filmsList: ResponseAPI) : RecyclerView.Adapter<MainAdapter.Adapter>() {
    class Adapter (val view: View): RecyclerView.ViewHolder(view)


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): Adapter {
        val layoutInflater = LayoutInflater.from(parent?.context)
        val cellForRow = layoutInflater.inflate(R.layout.film_item, parent, false)
        return Adapter(cellForRow)
    }

    override fun getItemCount(): Int {
        return filmsList.results.count()
    }

    override fun onBindViewHolder(holder: Adapter, position: Int) {
        // Response for the endpoint get all films
        val film = filmsList.results[position]

        //Inject data in the components at film_item.xml
        holder.view.episodeId.text = film.episodeId
        holder.view.title.text = film.title
        holder.view.director.text = film.director
    }
}