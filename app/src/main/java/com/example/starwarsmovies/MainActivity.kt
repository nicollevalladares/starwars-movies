package com.example.starwarsmovies

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.starwarsmovies.interfaces.ResponseAPI
import com.google.gson.GsonBuilder
import kotlinx.android.synthetic.main.activity_main.*
import okhttp3.*
import java.io.IOException


class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        //recycler view
        main_content.layoutManager = LinearLayoutManager(this)
        fetchData()
    }

    private fun fetchData () {
        val URL = "https://parseapi.back4app.com/classes/Film"

        //request to the endpoint with the respective auth
        val request = Request.Builder()
            .url(URL)
            .header("X-Parse-Application-Id", "kFuqGsemy2j84m8AfykdWikN2WdHEs45uGIFDV7F")
            .header("X-Parse-Master-Key", "mbUJqmLAMaVoASAkhmnOWf6am5qhmFXL5hcw0Ecf")
            .build()

        OkHttpClient().newCall(request).enqueue(object: Callback {
            override fun onResponse(call: Call, response: Response) {
                val body = response.body()?.string()
                val parser = GsonBuilder().create()

                val data = parser.fromJson(body, ResponseAPI::class.java)

                runOnUiThread {
                    //recycler view
                    main_content.adapter = MainAdapter(data)
                }
            }

            override fun onFailure(call: Call, e: IOException) {
                println("Service call failed for some reason")
            }
        })
    }
}
